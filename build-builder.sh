#!/bin/bash
set -euo pipefail
CURDIR="$(cd `dirname $0` && pwd)"

# The following set of variables must be defined to run this script.
VAR_NAME_ERROR="Variable must be defined to run this script"
: ${BUILDER_NAME:?${VAR_NAME_ERROR}}
: ${BUILDER_IMAGE:?${VAR_NAME_ERROR}}
: ${BUILDER_DOCKERFILE:?${VAR_NAME_ERROR}}

# Build the builder image
docker build \
    --rm \
    --pull \
    --tag ${BUILDER_IMAGE} \
    --file ${BUILDER_DOCKERFILE} \
    $(pwd)
