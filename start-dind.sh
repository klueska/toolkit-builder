#!/bin/bash
set -euo pipefail
CURDIR="$(cd `dirname $0` && pwd)"

# The following set of variables must be defined to run this script.
VAR_NAME_ERROR="Variable must be defined to run this script"
: ${DIND_NAME:?${VAR_NAME_ERROR}}

# Start the DIND on the bridge network by default.
: ${DIND_NETWORK:="bridge"}

# Conditionally start a dind instance for serving
# the docker daemon on tcp://localhost:2375
# of the ${DIND_NETWORK} network.
if [ ! "$(docker ps -q -f name=${DIND_NAME})" ]; then
    if [ "$(docker ps -aq -f status=exited -f name=${DIND_NAME})" ]; then
        docker rm ${DIND_NAME}
    fi

    docker run \
        -d \
        --rm \
        --privileged \
	    --network=${DIND_NETWORK} \
        -e DOCKER_HOST="tcp://${DIND_NAME}:2375" \
        -e DOCKER_TLS_CERTDIR="" \
        --name ${DIND_NAME} \
        docker:dind --experimental
fi
