#!/bin/bash
set -euo pipefail
CURDIR="$(cd `dirname $0` && pwd)"

# The following set of variables must be defined to run this script.
VAR_NAME_ERROR="Variable must be defined to run this script"
: ${BUILDER_IMAGE:?${VAR_NAME_ERROR}}
: ${LIB_NAME:?${VAR_NAME_ERROR}}

# Set the DIND name
DIND_NAME="${LIB_NAME}-dind"

# Run the builder to check the image generated for LIB_NAME
docker run \
    --rm \
    --link ${DIND_NAME}:${DIND_NAME} \
    -e DOCKER_HOST="tcp://${DIND_NAME}:2375" \
    -e DOCKER_TLS_CERTDIR="" \
    ${BUILDER_IMAGE} bash -c \
	    "export IMAGES=\$(docker image ls | grep nvidia/${LIB_NAME} | cut -d' ' -f1); \
	     for i in \${IMAGES}; do \
	         printf '%-60s: %s\n' \${i} \$(docker run \${i} uname -m); \
	     done"

