BUILDER := toolkit-builder
LIBS := libnvidia-container nvidia-container-toolkit

export BUILDER_NAME = $(BUILDER)
export BUILDER_IMAGE = nvidia/$(BUILDER)
export BUILDER_DOCKERFILE = $(CURDIR)/Dockerfile.builder
export DIST_DIR = $(CURDIR)/dist

all: $(LIBS)

builder:
	$(CURDIR)/build-builder.sh

$(LIBS): builder
	export LIB_NAME=$(@); \
	export LIB_PATH=$(CURDIR)/$(@); \
	export RHEL_CREDENTIALS_FILE="$(shell readlink -f $(CURDIR)/rhel-credentials.env)"; \
	$(CURDIR)/build-library.sh

check-images-%: LIB_NAME = $*
check-images-%:
	@export LIB_NAME=$(LIB_NAME); \
	 echo "Checking 'uname -m' for build images of '$(LIB_NAME)'"; \
	 $(CURDIR)/check-images.sh

$(patsubst %,check-images-%,$(LIBS)):
check-images: $(patsubst %,check-images-%,$(LIBS))

clean-%: DIND = $*-dind
clean-%: 
	@if [ "$$(docker ps -q -f name="$(DIND)")" != "" ]; then \
	     docker kill $${dind}; \
	 fi

$(patsubst %,clean-%,$(LIBS)): 
clean: $(patsubst %,clean-%,$(LIBS))

distclean:
	rm -rf $(DIST_DIR)
