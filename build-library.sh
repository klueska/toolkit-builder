#!/bin/bash
set -euo pipefail
CURDIR="$(cd `dirname $0` && pwd)"

# The following set of variables must be defined to run this script.
VAR_NAME_ERROR="Variable must be defined to run this script"
: ${BUILDER_IMAGE:?${VAR_NAME_ERROR}}
: ${LIB_NAME:?${VAR_NAME_ERROR}}
: ${LIB_PATH:?${VAR_NAME_ERROR}}
: ${RHEL_CREDENTIALS_FILE?=${VAR_NAME_ERROR}}
: ${DIST_DIR?=${VAR_NAME_ERROR}}

# Set the default BUILD_TARGETS
: ${BUILD_TARGETS:="docker-all"}

# Set the CONTAINER_LIB_PATH
HOST_LIB_PATH="${LIB_PATH}"
CONTAINER_LIB_PATH="/${LIB_NAME}"

# Set the CONTAINER_RHEL_CREDENTIALS_FILE
HOST_RHEL_CREDENTIALS_FILE="${RHEL_CREDENTIALS_FILE}"
CONTAINER_RHEL_CREDENTIALS_FILE="/rhel-credentials.env"

# Set the CONTAINER_DEST_DIR
HOST_DIST_DIR="${DIST_DIR}"
CONTAINER_DIST_DIR="/${LIB_NAME}-dist"

# Start a DIND container for building the builder image
export DIND_NAME="${LIB_NAME}-dind"
${CURDIR}/start-dind.sh

# Run the builder for LIB_NAME being served by DIND_NAME
docker run \
    --rm \
    --link ${DIND_NAME}:${DIND_NAME} \
    -e DOCKER_HOST="tcp://${DIND_NAME}:2375" \
    -e DOCKER_TLS_CERTDIR="" \
    -e RHEL_CREDENTIALS_FILE="${CONTAINER_RHEL_CREDENTIALS_FILE}" \
    -e DIST_DIR="${CONTAINER_DIST_DIR}" \
    -v ${HOST_LIB_PATH}:${CONTAINER_LIB_PATH} \
    -v ${HOST_RHEL_CREDENTIALS_FILE}:${CONTAINER_RHEL_CREDENTIALS_FILE} \
    -v ${HOST_DIST_DIR}:${CONTAINER_DIST_DIR} \
    -w ${CONTAINER_LIB_PATH} \
    ${BUILDER_IMAGE} make -j 1 ${BUILD_TARGETS}
